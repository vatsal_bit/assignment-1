Step 1:
Open '.sln' file to open the solution in Visual Studio.

Step 2:
Build and run the solution.

Step 3:
Enter two numbers (nodes) to find the distance.

Step 4:
The result will be the distance between two nodes entered.

Note:
Negative numbers and alphabets are not allowed.